// problem1.js
function findCarById(inventory, carId) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === carId) {
            const car = inventory[i];
            const result = `Car ${carId} is a ${car.car_year} ${car.car_make} ${car.car_model}`;
            return result;
        }
    }
    return `Car with id ${carId} not found in inventory`;
}

module.exports = findCarById;
