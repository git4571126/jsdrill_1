function sortCarModelsAlphabetically(inventory) {
    // Initialize an empty array to store car models
    const carModels = [];

    // Use a for...of loop to iterate through each car object
    for (const car of inventory) {
        carModels.push(car.car_model);
    }

    // Sort alphabetically
    carModels.sort();

    // Log the sorted array to console
    console.log(carModels);
}

module.exports = sortCarModelsAlphabetically;
