function getBMWAndAudiCars(inventory) {
    // Filter BMW and Audi cars
    const BMWAndAudi = inventory.filter(car => car.car_make === 'BMW' || car.car_make === 'Audi');

    // Convert array to JSON string
    const jsonBMWAndAudi = JSON.stringify(BMWAndAudi);

    // Log the JSON string to console
    console.log(jsonBMWAndAudi);

    // Return the array of BMW and Audi cars (optional, as not explicitly required by the problem statement)
    return BMWAndAudi;
}

module.exports = getBMWAndAudiCars;
