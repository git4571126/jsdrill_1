function getAllCarYears(inventory) {
    // Initialize an empty array to store car years
    const carYears = [];

    // Use a for...of loop to iterate 
    for (const car of inventory) {
        carYears.push(car.car_year);
    }

    // Log the array of car years to console
    console.log(carYears);
}

module.exports = getAllCarYears;
