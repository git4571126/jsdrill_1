
const getAllCarYears = require('./problem4'); // Import the function from Problem #4

function carsOlderThan2000(inventory) {
    // Get array of all car years using the function from Problem #4
    const carYears = getAllCarYears(inventory);

    // Check if carYears is undefined or empty before proceeding
    if (!carYears || carYears.length === 0) {
        console.log("No car years found in inventory.");
        return []; // Return empty array or handle appropriately
    }

    // Filter cars made before the year 2000
    const olderCars = carYears.filter(year => year < 2000);

    // Log the array of older cars
    console.log(olderCars);

    // Log the length of the array (number of older cars)
    console.log(`Number of cars older than 2000: ${olderCars.length}`);

    // Return the array of older cars (optional, as not explicitly required by the problem statement)
    return olderCars;
}

module.exports = carsOlderThan2000;
