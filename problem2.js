function getLastCarInfo(inventory) {
    // Get the index of the last car in the array
    const lastIndex = inventory.length - 1;
    
    // Retrieve the last car object
    const lastCar = inventory[lastIndex];
    
    // Format the output string
    const result = `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
    
    return result;
}

module.exports = getLastCarInfo;
