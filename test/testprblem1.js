// test/testProblem1.js
const findCarById = require('../problem1');
const inventory = require('./inventory'); // Adjust the path if necessary

const carId = 33;
const result = findCarById(inventory, carId);
console.log(result); // Ensure this line is present to output the result
